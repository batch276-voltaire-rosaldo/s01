name = "Jane"
age = 23
occupation = "FE Dev"
movie = "Iron Man"
rating = 95.0

num1 = 3
num2 = 4
num3 = 5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

print(num1*num2)
print(num1 < num3)
print(num3 + num2)