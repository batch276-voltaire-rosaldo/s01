# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol


# [Section] Python Syntax
# Hello World in Python
print("Hello World!")

# [Section] Indentation
# Where in other programming languages the indentation in code is for readability only, the indentation in Python is very important
# In Python, indentation is used to indicated a block of code
# Similar to JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the container of data
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter (A to Z or a to z), dollar sign or an underscore.
# after the first character, identifier can have any combination of characters
# Unlike JS that uses the came casing, Python uses the snake case convention for variables as defined in the PEP (Python enhancement proposal).
# Most importantly, identifiers are case sensitive.
age = 35
middle_initial = "C"
# Python allows assigning values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# In Python, there are commonly used data types:

full_name = "John Doe"
secret_code = "Pa$$word"


num_of_days = 365
pi_approx = 3.1416
complex_num = 1 + 5j

print(type(complex_num))

# 3. Boolean(bool) - for truth values

isLearning = True
isDifficult = False

print(full_name + " " + secret_code)

# print("My age is " + age)

# [Section] Typecasting
# here are some functions that can be use in typecasting
print(int(3.75))
print(float(3.75))
print("My age is " + str(age))

print(f"Hi my name is {full_name} and my age is {age}.")

# [Section] Operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators
num1 = 4
num1 += 3
print(num1)

print(1 == "1")





